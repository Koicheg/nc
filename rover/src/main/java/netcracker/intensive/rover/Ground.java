package netcracker.intensive.rover;

public class Ground {
    private int length;
    private int width;

    public int getLength() {
        return length;
    }
    public int getWidth() {
        return width;
    }

    private GroundCell[][] land;

    public Ground(int width, int length) {
        this.width = width;
        this.length = length;
        this.land = new GroundCell[width][length];
    }

    public GroundCell getCell(int x, int y) throws OutOfGroundException {
        if (x >= 0 && x < width && y >= 0 && y < length)
            return land[x][y];
        else  throw new OutOfGroundException();
    }

    public void initialize(GroundCell... args) {
        if (args.length < width * length) {
            throw new IllegalArgumentException();
        }
        else {
            int n = 0;
            for (int y = 0; y < length; y++) {
                for (int x = 0; x < width; x++) {
                    land[x][y] = args[n];
                    n++;
                }
            }
        }
    }
}