package netcracker.intensive.rover;
import netcracker.intensive.rover.constants.Direction;


public class Rover implements Landable, Liftable, Moveable, Turnable {

    public Point getPosition() {
        return position;
    }

    private Point position;
    private boolean airborne;
    private Direction direction;
    private GroundVisor groundVisor;
    private int tmp = 0;

    public Rover(GroundVisor groundVisor) {
        this.groundVisor = groundVisor;
        position = new Point(0, 0);
        direction = Direction.SOUTH;

    }

    public void turnTo(Direction direction) {
        this.direction = direction;
    }
    public Point getCurrentPosition() {
        return position;
    }
    public boolean isAirborne() {
        return airborne;
    }
    public Direction getDirection() {
        return direction;
    }

    @Override
    public void lift() {
        airborne = true;
        position = null;
        direction = null;
    }
    @Override
    public void land(Point position, Direction direction){
        try {
            if (!groundVisor.hasObstacles(position)) {
                airborne = false;
                this.position = position;
                this.direction = direction;
            }
        } catch (OutOfGroundException e) {}
    }
    @Override
    public void move() {
        if (position != null) {
            Point nextPosition = new Point(position);
            // Point prevPosition = position;
            if (Direction.EAST.equals(direction)) {
                tmp = position.getX();
                nextPosition.setX(++tmp);
            } else if (Direction.WEST.equals(direction)) {
                tmp = position.getX();
                nextPosition.setX(--tmp);
            } else if (Direction.NORTH.equals(direction)) {
                tmp = position.getY();
                nextPosition.setY(--tmp);
            } else if (Direction.SOUTH.equals(direction)) {
                tmp = position.getY();
                nextPosition.setY(++tmp);
            }
            if (!airborne) {
                try {
                    if (!groundVisor.hasObstacles(nextPosition)) {
                        position = nextPosition;
                    }

                } catch (OutOfGroundException e) {
                    lift();
                }
            }
        }
    }
}
