package netcracker.intensive.rover.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingCommand implements RoverCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingCommand.class);
    private RoverCommand roverCommand;

    public LoggingCommand(RoverCommand roverCommand) {
        this.roverCommand = roverCommand;
    }
    public void execute(){
        roverCommand.execute();
        LOGGER.info(roverCommand.toString());
    }

    @Override
    public String toString() {
        return roverCommand.toString();
    }
}
