package netcracker.intensive.rover.programmable;
import netcracker.intensive.rover.GroundVisor;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.command.RoverCommand;
import netcracker.intensive.rover.stats.SimpleRoverStatsModule;

import java.util.Map;

/**
 * Этот класс должен уметь все то, что умеет обычный Rover, но при этом он еще должен уметь выполнять программы,
 * содержащиеся в файлах
 */
public class ProgrammableRover extends Rover implements ProgramFileAware {
    private SimpleRoverStatsModule simpleRoverStatsModule;
    private RoverProgram program;

    public ProgrammableRover(GroundVisor groundVisor, SimpleRoverStatsModule simpleRoverStatsModule) {
        super(groundVisor);
        this.simpleRoverStatsModule = simpleRoverStatsModule;
    }

    public Map<String,Object> getSettings(){
        return program.getSettings();
    }

    public void executeProgramFile(String file) {
        RoverCommandParser roverCommandParser = new RoverCommandParser(this, file);
        program = roverCommandParser.getProgram();
        if ((boolean) program.getSettings().get(RoverProgram.STATS)) {         //check stats on
            simpleRoverStatsModule.registerPosition(getCurrentPosition()); // write starting position
        }

        for (RoverCommand command : program.getCommands()) {
            command.execute();
            if ((boolean) program.getSettings().get(RoverProgram.STATS)) {         //check stats on
                simpleRoverStatsModule.registerPosition(getCurrentPosition()); // write all new position
            }
        }

    }

    //for sout simpleRoverStatsModule.VisitedPoints
    @Override
    public String toString() {
        return simpleRoverStatsModule.toString();
    }
}
