package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.command.*;
import netcracker.intensive.rover.constants.Direction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class RoverCommandParser {

    private ProgrammableRover rover;
    private RoverProgram programm;
    private String file;

    public RoverCommandParser(ProgrammableRover rover, String file) {
        this.rover = rover;
        this.file = file;
        programm = new RoverProgram();
    }

    public RoverProgram getProgram() {
        try {
            BufferedReader in = new BufferedReader(new FileReader(this.getClass().getResource(this.file).getFile()));
            RoverCommand command;
            try {
                String s;
                while ((s = in.readLine()) != null) {
                    //check log on/off
                    if (s.contains("log")) {
                        String[] parse = s.split(" ");
                        if ("on".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], true);
                            continue;
                        } else if ("off".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], false);
                            continue;
                        }
                    }
                    //check stats on/off
                    if (s.contains("stats")) {
                        String[] parse = s.split(" ");
                        if ("on".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], true);
                            continue;
                        } else if ("off".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], false);
                            continue;
                        }
                    }
                    // check separator ===
                    if (s.equals(RoverProgram.SEPARATOR)) {
                        while ((s = in.readLine()) != null) {
                            //read command from file
                            programm.commandList.add("\n" + s);
                            command = readCommand(s);
                            if ((boolean) programm.getSettings().get(RoverProgram.LOG)) {
                                programm.commands.add(new LoggingCommand(command));   //add with logginer when log on
                            } else programm.commands.add(command);
                        }
                    }
                }
            } finally {
                in.close();
            }
        }
        catch(NullPointerException e) {
            throw new RoverCommandParserException();
        }
        catch(IOException e) {
            throw new RoverCommandParserException();
        }
        return programm;
    }

    private RoverCommand readCommand(String str) {
        String[] parse = str.split(" ");
        RoverCommand command = null;
        if("move".equals(parse[0])){
            command = new MoveCommand(this.rover);
        }
        else if("turn".equals(parse[0])){
            command = new TurnCommand(this.rover, getDirection(parse[1]));

        }
        else if("lift".equals(parse[0])){
            command = new LiftCommand(this.rover);
        }
        else if("land".equals(parse[0])){
            command = new LandCommand(this.rover, newPoint(parse[1], parse[2]), getDirection(parse[3]));
        }
        return command;
    }

    private Direction getDirection(String str) {
        Direction direction = null;
        if ("west".equalsIgnoreCase(str)){
            direction = Direction.WEST;
        }
        else if ("east".equalsIgnoreCase(str)){
            direction = Direction.EAST;
        }
        else if ("north".equalsIgnoreCase(str)){
            direction = Direction.NORTH;
        }
        else if ("south".equalsIgnoreCase(str)){
            direction = Direction.SOUTH;
        }
        return direction;
    }

    private Point newPoint(String a, String b){
        Point point = new Point(0,0);
        point.setX(Integer.parseInt(a));
        point.setY(Integer.parseInt(b));
        return point;
    }


    public RoverProgram getProgramGUI() {
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            RoverCommand command;
            try {
                String s;
                while ((s = in.readLine()) != null) {
                    //check log on/off
                    if (s.contains("log")) {
                        String[] parse = s.split(" ");
                        if ("on".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], true);
                            continue;
                        } else if ("off".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], false);
                            continue;
                        }
                    }
                    //check stats on/off
                    if (s.contains("stats")) {
                        String[] parse = s.split(" ");
                        if ("on".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], true);
                            continue;
                        } else if ("off".equalsIgnoreCase(parse[1])) {
                            programm.settings.put(parse[0], false);
                            continue;
                        }
                    }
                    // check separator ===
                    if (s.equals(RoverProgram.SEPARATOR)) {
                        while ((s = in.readLine()) != null) {
                            //read command from file
                            programm.commandList.add("\n" + s);
                            command = readCommand(s);
                            if ((boolean) programm.getSettings().get(RoverProgram.LOG)) {
                                programm.commands.add(new LoggingCommand(command));   //add with logginer when log on
                            } else programm.commands.add(command);
                        }
                    }
                }
            } finally {
                in.close();
            }
        }
        catch(NullPointerException e) {
            throw new RoverCommandParserException();
        }
        catch(IOException e) {
            throw new RoverCommandParserException();
        }
        return programm;
    }
}
