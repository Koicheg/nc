package netcracker.intensive.rover.programmable.RoverGUI;
import javax.swing.*;
import java.awt.*;

/**
 * Created by koicheg on 27.01.17.
 */
public class Picture {
    private Image ground;
    private Image groundWithWall;
    private Image roverEAST;
    private Image roverWEST;
    private Image roverSOUTH;
    private Image roverNORTH;
    private ImageIcon img;

    public Picture() {
        img = new ImageIcon("/home/koicheg/java-nc-intensive/rover/src/test/resources/netcracker/intensive/rover/programmable/ground.png");
        ground = img.getImage();
        img = new ImageIcon("/home/koicheg/java-nc-intensive/rover/src/test/resources/netcracker/intensive/rover/programmable/groundWithWall.png");
        groundWithWall = img.getImage();
        img = new ImageIcon("/home/koicheg/java-nc-intensive/rover/src/test/resources/netcracker/intensive/rover/programmable/roverEAST.png");
        roverEAST = img.getImage();
        img = new ImageIcon("/home/koicheg/java-nc-intensive/rover/src/test/resources/netcracker/intensive/rover/programmable/roverWEST.png");
        roverWEST = img.getImage();
        img = new ImageIcon("/home/koicheg/java-nc-intensive/rover/src/test/resources/netcracker/intensive/rover/programmable/roverSOUTH.png");
        roverSOUTH = img.getImage();
        img = new ImageIcon("/home/koicheg/java-nc-intensive/rover/src/test/resources/netcracker/intensive/rover/programmable/roverNORTH.png");
        roverNORTH = img.getImage();
    }
    public Image getGround(){
        return ground;
    }
    public Image getGroundWithWall(){
        return groundWithWall;
    }
    public Image getRoverEast(){
        return roverEAST;
    }
    public Image getRoverSOUTH(){
        return roverSOUTH;
    }
    public Image getRoverWEST(){
        return roverWEST;
    }
    public Image getRoverNORTH(){
        return roverNORTH;
    }

}
