package netcracker.intensive.rover.programmable.RoverGUI;

import netcracker.intensive.rover.Ground;
import netcracker.intensive.rover.GroundCell;
import netcracker.intensive.rover.GroundVisor;
import netcracker.intensive.rover.OutOfGroundException;
import netcracker.intensive.rover.command.RoverCommand;
import netcracker.intensive.rover.constants.CellState;
import netcracker.intensive.rover.constants.Direction;
import netcracker.intensive.rover.programmable.ProgrammableRover;
import netcracker.intensive.rover.programmable.RoverCommandParser;
import netcracker.intensive.rover.programmable.RoverProgram;
import netcracker.intensive.rover.stats.SimpleRoverStatsModule;

import javax.swing.*;
import java.awt.*;

/**
 * Created by koicheg on 27.01.17.
 */
public class RoverGUI {
    JFrame groundFrame;
    JTextArea text;
    Board groundPanel;
    Image cellImage;
    String file = "program1.txt";

    //Test ProgrammableRover on 3x3 Ground

    public static void main(String[] args) {
        new RoverGUI();
    }

    public RoverGUI() {
        Picture pic = new Picture();
        groundPanel = new Board();
        groundFrame = new JFrame();
        groundFrame.setLayout(new BorderLayout());
        groundFrame.setTitle("Rover Visualization");
        groundFrame.setSize(400, 700);
        groundFrame.setLocationRelativeTo(null);
        groundFrame.add(groundPanel, BorderLayout.CENTER);
        text = new JTextArea("");
        groundFrame.add(text, BorderLayout.SOUTH);
        groundFrame.setVisible(true);
        groundFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        groundPanel.executeRoverCommand();
    }

    public class Board extends JPanel {
        Picture pic = new Picture();
        private String file = "/home/koicheg/java-nc-intensive/rover/src/test/resources/netcracker/intensive/rover/programmable/program1.txt";
        private ProgrammableRover roverGUI;
        private RoverProgram program;
        private RoverCommandParser roverCommandParser;

        Ground ground = new Ground(4, 4) {{
            initialize(new GroundCell(CellState.FREE), new GroundCell(CellState.FREE), new GroundCell(CellState.FREE),
                    new GroundCell(CellState.FREE), new GroundCell(CellState.OCCUPIED), new GroundCell(CellState.FREE),
                    new GroundCell(CellState.FREE), new GroundCell(CellState.OCCUPIED), new GroundCell(CellState.FREE),
                    new GroundCell(CellState.FREE), new GroundCell(CellState.OCCUPIED), new GroundCell(CellState.FREE),
                    new GroundCell(CellState.FREE), new GroundCell(CellState.OCCUPIED), new GroundCell(CellState.FREE),
                    new GroundCell(CellState.FREE));
        }};
        public Board(){
            roverGUI = new ProgrammableRover(new GroundVisor(ground), new SimpleRoverStatsModule());
            roverCommandParser = new RoverCommandParser(roverGUI, file);
            program = roverCommandParser.getProgramGUI();
        }

        public void executeRoverCommand(){
            text.setText(program.getCommandList().toString());
            for (RoverCommand command : program.getCommands()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                command.execute();
                if(roverGUI.getPosition()!=null) {
                    repaint();
                }
            }
        }

        public void paint(Graphics g) {
            super.paint(g);
            int scale = 100;

            for (int y = 0; y < ground.getLength(); y++) {
                for (int x = 0; x < ground.getWidth(); x++) {
                    try {
                        if (ground.getCell(x,y).getState().equals(CellState.FREE)) {
                            cellImage = pic.getGround();
                            g.drawImage(cellImage, x * scale, y * scale, null);
                        }
                        else if (ground.getCell(x,y).getState().equals(CellState.OCCUPIED)) {
                            cellImage = pic.getGroundWithWall();
                            g.drawImage(cellImage, x * scale, y * scale, null);
                        }
                    } catch (OutOfGroundException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (roverGUI.getDirection().equals(Direction.EAST))
                g.drawImage(pic.getRoverEast(), roverGUI.getPosition().getX() * scale, roverGUI.getPosition().getY() * scale, null);
            else if (roverGUI.getDirection().equals(Direction.WEST))
                g.drawImage(pic.getRoverWEST(), roverGUI.getPosition().getX() * scale, roverGUI.getPosition().getY() * scale, null);
            else   if (roverGUI.getDirection().equals(Direction.SOUTH))
                g.drawImage(pic.getRoverSOUTH(), roverGUI.getPosition().getX() * scale, roverGUI.getPosition().getY() * scale, null);
            else   if (roverGUI.getDirection().equals(Direction.NORTH))
                g.drawImage(pic.getRoverNORTH(), roverGUI.getPosition().getX() * scale, roverGUI.getPosition().getY() * scale, null);
        }
    }
}

