package netcracker.intensive.rover.programmable;


import netcracker.intensive.rover.command.RoverCommand;
import java.util.*;

public class RoverProgram {
    public static final String LOG = "log";
    public static final String STATS = "stats";
    public static final String SEPARATOR = "===";
    protected Map<String, Object> settings;
    protected ArrayList<RoverCommand> commands;

    protected ArrayList<String> commandList;



    public RoverProgram() {
        settings = new HashMap<>();
        commands = new ArrayList<>();
        commandList = new ArrayList<>();
    }

    public ArrayList<String> getCommandList() {
        return commandList;
    }
    public Map<String, Object> getSettings() {
        return Collections.unmodifiableMap(settings);
    }

    public ArrayList<RoverCommand> getCommands(){
        return commands;
    }

    public void settings(Map<String, Object> settings) {
        this.settings = settings;
    }
}

