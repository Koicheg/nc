package netcracker.intensive.rover.stats;

import netcracker.intensive.rover.Point;
import java.util.Set;

public interface RoverStatsModule {
    void registerPosition(Point position);
    boolean isVisited(Point point);
    Set<Point> getVisitedPoints();
}
