package netcracker.intensive.rover.stats;


import netcracker.intensive.rover.Point;

import java.util.HashSet;
import java.util.Set;

public class SimpleRoverStatsModule implements RoverStatsModule {

    private HashSet<Point> visitedPoints;
    // for sout test ProgrammableRover
    @Override
    public String toString() {
        String s = "";
        for(Point p: visitedPoints){
            if (p==null){
                s+= " land ";
            }
            else
                s+=(p.toString());
        }
        if (s!=""){
            return "Visited point: " + s;
        }
        else return "stats off, we do not write the visited point";
    }

    public SimpleRoverStatsModule() {
        visitedPoints = new HashSet<>();
    }

    @Override
    public void registerPosition(Point position) {
        if(!isVisited(position)){
            visitedPoints.add(position);
        }
    }
    @Override
    public boolean isVisited(Point position) {
        return visitedPoints.contains(position);
    }

    @Override
    public Set<Point> getVisitedPoints() {

        return visitedPoints;
    }

}

